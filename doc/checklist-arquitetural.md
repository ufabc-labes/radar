# Checklist arquitetural

Algumas verificações são bem difíceis de automatizar, então vai aqui esse checklist.

## Experiência do desenvolvedor

* Uso de automação para: 1) minimizar o esforço de montar o ambiente local e 2) deixar o ambiente local o mais parecido possível com produção.
* A menos de determinadas configurações, uma alteração no código-fonte deve se refletir automaticamente na instância em execução sem que o desenvolvedor tenha que executar algum comando (muito menos reiniciar a aplicação).

## Logs

* Fora do ambiente produtivo: registrar logs a partir do nível DEBUG.
* No ambiente produtivo: registrar logs a partir do nível INFO.
* Deve haver log em arquivo (persistente à reinicialização dos containeres).
* Os jobs (executados com runcrons) devem ter seus logs também registrados no arquivo de logs.
* Exceções sempre devem ser logadas (em qualquer ambiente, e mesmo nos jobs).

## Cache

* Somente em produção - duração infinita (mas na prática atualizado diariamente pelo job CashRefresherJob).
* Somente para a análise PCA, que é algo bem demorado.
* Configurar cache para suportar itens com até 10mb (json da cdep tem mais de 1mb, que é o limite padrão).
