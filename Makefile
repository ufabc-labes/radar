SHELL := /bin/bash
###############################################################################
#                           RADAR RELATED VARIABLES                           #
###############################################################################
RUN_CMD = bash scripts/ci/run_ci.sh

RADAR_IMAGE := $(shell . scripts/variables.sh; echo $${IMAGEM_TESTE}:$${TAG})
RADAR_NGINX_IMAGE := $(shell . scripts/variables.sh; echo $${IMAGEM_NGINX}:$${TAG})

# Variáveis usadas nos bkps
DATE_PREFIX = $(shell date +'%Y_%m_%d-%Hh')
DB_BKP_FILE := bkps/${DATE_PREFIX}_dump.json
MEDIA_BKP := bkps/${DATE_PREFIX}_media.tar


# Para o caso de o comand/target ter sido "teste", precisamos definir qual será
# o teste a ser executado.
ifeq (${T}, migrations)
	TESTE := testa_migrations_esquecidas
else
	TESTE := testa_aplicacao
endif

# Para o caso de o comand/target ter sido "linter", precisamos definir qual será
# o linter a ser executado.
ifeq (${L}, http_link)
	LINTER := linter http_link
else
	LINTER := linter shellcheck
endif

####################################
# Definindo os targets do Makefile #
####################################
.DEFAULT_GOAL : help

help:
	@echo "Bem vindo(a) ao Makefile do Radar Parlamentar"
	@echo ""
	@echo "Este arquivo tem o objetivo de facilitar a vida da pessoa desenvolvedora no"
	@echo "fluxo de trabalho com o Radar. Abaixo você encontrará as opções disponíveis:"
	@echo ""
	@echo "    build"
	@echo "        Faz o build das imagens docker (teste e nginx)."
	@echo ""
	@echo "    iniciar"
	@echo "        Inicia a stack de serviços do radar."
	@echo ""
	@echo "    parar"
	@echo "        Para e remove a stack de serviços do radar."
	@echo ""
	@echo "    atualizar"
	@echo "        Atualiza a stack do radar (faz redeploy se preciso)."
	@echo ""
	@echo "    teste [T=<teste>]"
	@echo "        Executa algum dos nossos testes."
	@echo "        As opções de testes que temos são:"
	@echo "            - aplicacao (default): Roda todos os testes do django."
	@echo "            - migrations: verifica se está faltando alguma migration."
	@echo ""
	@echo "    shell_django"
	@echo "        Abre o shell do Django"
	@echo ""
	@echo "    psql"
	@echo "        Acessa o banco de dados via psql"
	@echo ""
	@echo "    logs"
	@echo "        Visualiza os logs da aplicação (tanto container Django quanto Celery)"
	@echo "        Não inclui log de requisições"
	@echo ""
	@echo "    logs_django"
	@echo "        Visualiza os logs do container Django; inclui log de requisições"
	@echo ""
	@echo "    logs_celery"
	@echo "        Visualiza os logs do container Celery; inclui log de requisições"
	@echo ""
	@echo "    linter L=<teste>"
	@echo "        Executa teste de linter."
	@echo "        As opções que temos são:"
	@echo "            - shellcheck (default): Roda todos os testes do django."
	@echo "            - http_link."
	@echo ""
	@echo "    build_ci"
	@echo "        Faz o build da imagen a ser usada no CI"
	@echo ""
	@echo "    clean"
	@echo "        Limpezas relacionadas ao docker (prune):"
	@echo "            - imagens: Layers não utilizados em imagens 'finais'."
	@echo "            - containers que não estão em execução."
	@echo "            - Volumes e redes (networks) não associados a containers em execução."
	@echo ""
	@echo "    clean_dev_images"
	@echo "        Remove todas as imagens do radar com tag 'dev-local'."
	@echo ""
	@echo "    clean-repo"
	@echo "        Remove todos arquivos temporários, modificados ou não comitados no git."
	@echo ""
	@echo "    dump-database"
	@echo "        Faz dump da base de dados do projeto para o diretório 'bkps/' prefixado com date-time atual."
	@echo ""
	@echo "    backup"
	@echo "        Faz backup tanto da base de dados quanto os arquivos de mídia no diretório 'bkps/'."
	@echo ""
	@echo "    help"
	@echo "        Mostra essa mensagem de ajuda"
.PHONY: help

# Inicia o swarm caso ele ainda não esteja iniciado
# Em seguida inicializa (ou atualiza) a stack do radar
iniciar: build
	@bash scripts/helpers.sh swarm_init 2>&1 /dev/null
	@docker stack deploy -c radar.yml -c radar_dev.yml radar
.PHONY: iniciar

# Para completamente a stack do radar e também o docker swarm, se não tiver
# outra stack rodando
parar:
	@bash scripts/helpers.sh swarm_stop
	@docker run -d --rm -v "$$(pwd):/radar" alpine:latest sh -c 'find /radar -type f \( -name "*.pyc" -or -name "*.pyo" \) -delete && find /radar -type d -name "__pycache__" -exec rm -fr {} +'
.PHONY: parar

# Só um wrapper simbólico, já que o resultado do iniciar é o mesmo
atualizar: iniciar
.PHONY: atualizar

build:
	${RUN_CMD}  nginx_build
	${RUN_CMD}  teste_build
.PHONY: build

build_ci:
	@${RUN_CMD} ci_build
.PHONY: build_ci

teste: build
	@${RUN_CMD} ${TESTE}
.PHONY: teste

linter:
	@${RUN_CMD} ${LINTER}
.PHONY: teste

shell_django:
	@docker exec -it $(shell docker ps --filter name=radar_django -q) python manage.py shell
.PHONY: django_shell

psql:
	@docker exec -it $(shell docker ps --filter name=radar_postgres -q) psql -U radar
.PHONY: psql

logs:
	@docker exec -it $(shell docker ps --filter name=radar_django -q) tail -n 100 -f  /var/log/radar/radar.log
.PHONY: django_logs

logs_django:
	@docker service logs --tail 100 -f radar_django
.PHONY: django_logs

logs_celery:
	@docker service logs --tail 100 -f radar_celery
.PHONY: celery_logs

clean:
	docker volume prune -f
	docker network prune -f
	docker container prune -f
	docker image prune -f
.PHONY: clean

clean_dev_images:
	@echo "Removendo imagens com formato '*radar-parlamentar*:dev-local'"
	@docker image ls | grep "radar-parlamentar" | grep 'dev-local' | awk '{ print $$3 }' | xargs -r docker rmi -f
	@echo "Removendo layers pendentes"
	@docker image prune -f
.PHONY: clean_dev_images

clean_py_temp_files:
	@docker run -d --rm -v "$$(pwd):/radar" alpine:latest sh -c 'find /radar -type f \( -name "*.pyc" -or -name "*.pyo" \) -delete && find /radar -type d -name "__pycache__" -exec rm -fr {} +'
.PHONY: clean_py_temp_files

clean-repo: clean_py_temp_files
	git reset --hard HEAD
	git clean -di -e '.env'
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +
.PHONY: clean-repo

dump-database:
	docker exec $(docker ps --filter name=django -q) /bin/bash -c "python manage.py dumpdata --natural-foreign -e sessions -e admin -e contenttypes -e auth.Permission > /radar/$(DB_BKP_FILE)"
	bzip2 -9 -f $(DB_BKP_FILE)
.PHONY: dump-database

backup: dump-database
	tar -cf $(MEDIA_BKP) radar_parlamentar/static/media
.PHONY: backup
