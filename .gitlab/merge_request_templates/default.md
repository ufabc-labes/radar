## O que esse MR faz?

<!--
Descreva em detalhes o que o seu Merge Request faz, porque ele faz, etc.

Por favor, não se esqueça de manter esta descrição atualizada com qualquer
discussão que ocorra para que os revisores possam entender seu objetivo. Isto é
especialmente importante se eles não participarem de todas as discussões.

Isso também é importante pois esta descrição ficará registrada no histórico git
ajudando no futuro caso alguém precise voltar a este trabalho.

Não se esqueça de remover este comentário assim que você terminar.
-->

Adicione uma descrição ao seu Merge Request aqui.

## Qual/Quais Issue(s) esse Merge Request se propõe a resolver?

- Issue 1
- Issue 2

## Checklist geral

- [ ] A documentação do projeto foi atualizada?
- [ ] Testes foram adicionados/atualizados nas devidas áreas?
- [ ] O Merge Request está de acordo com as regras de estilo do projeto?
