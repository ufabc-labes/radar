"""Django settings for radar_parlamentar project."""
import logging
import os
from pathlib import Path

log = logging.getLogger("radar")

ADMINS = (('Leonardo', 'leonardofl87@gmail.com'),
          ('Diego', 'diraol@diraol.eng.br'))
MANAGERS = ADMINS

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
TIME_ZONE = 'America/Sao_Paulo'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'pt-br'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = '/static/'

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    '/radar/radar_parlamentar/radar_parlamentar/static',
)

COMPRESS_ENABLED = True
COMPRESS_ROOT = STATIC_ROOT
COMPRESS_OFFLINE = True
COMPRESS_CSS_FILTERS = (
    'compressor.filters.css_default.CssAbsoluteFilter',
    'compressor.filters.cssmin.CSSMinFilter',
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    # 'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
    # 'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = os.getenv('RADAR_SECRET_KEY',
                       '&amp;bt*nmd1d(+8*rm^nm9#0ge$iepd8!vw(#v9+z3!e9iel^ls8')

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            Path('radar_parlamentar/templates/').resolve(),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'radar_parlamentar.middleware.ExceptionLoggingMiddleware'
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'radar_parlamentar.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'radar_parlamentar.wsgi.application'

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.sitemaps',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_cron',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    'compressor',
    'cssmin',
    'jsmin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    'modelagem',
    'importadores',
    'analises',
    'exportadores',
    'testes_integracao',
    'radar_parlamentar',
    'plenaria',
)

# https://docs.python.org/pt-br/3/library/logging.html
# https://docs.djangoproject.com/en/3.2/topics/logging/
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'simple': {
            'format': '%(levelname)s %(asctime)s %(module)s.%(funcName)s: %(message)s',
            'datefmt': '%y-%m-%d %H:%M:%S'
        }
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'file': {
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'simple',
            'filename': '/var/log/radar/radar.log',
            'backupCount': 2,
            'maxBytes': 1024*1024*10,  # 10MB
        }
    },
    'loggers': {
        'radar': {
            'handlers': ['file', 'console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'radar-file-only': {
            'handlers': ['file'],
            'level': 'ERROR',
            'propagate': False,
        }
    }
}

ELASTIC_SEARCH_ADDRESS = {'host': 'elasticsearch', 'port': '9200'}
ELASTIC_SEARCH_INDEX = "radar_parlamentar"

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'radar',
        'USER': 'radar',
        'PASSWORD': os.getenv('RADAR_DB_PASSWORD', 'radar'),
        'HOST': 'postgres'
    }
}

ALLOWED_HOSTS = ['radarparlamentar.polignu.org', 'localhost', '127.0.0.1']

CELERY_BROKER_URL = 'amqp://guest:guest@rabbitmq:5672//'

DEBUG = True

# Fora de produção não queremos cache
CACHE_TIMEOUT = 0

if os.getenv('RADAR_IS_PRODUCTION'):

    log.info('Starting PRODUCTION environment ...')
    # Em produção:
    # Não tem log de debug
    # Erro 500 não mostra detalhes no navegador
    # Não tem expiração de cache global
    #   experição é feita pela aplicação
    # Tem jobs rodando

    DEBUG = False
    LOGGING['loggers']['radar']['level'] = 'INFO'

    CACHE_TIMEOUT = None  # sem expiração

    CRON_CLASSES = [
        # 'cron.jobs.DemoJob',  # job para debug do mecanismo batch
        'cron.jobs.ImportadorJob',
        'cron.jobs.CashRefresherJob',
        'cron.jobs.DbDumperJob',
    ]

if os.getenv('RADAR_TEST'):
    log.info('Starting TEST environment ...')
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': 'radar_parlamentar.db',
        }
        # 'default': {
        #     'ENGINE': 'django.db.backends.postgresql_psycopg2',
        #     'NAME': 'radar',
        #     'USER': 'radar',
        #     'PASSWORD': 'radar',
        #     'HOST': 'test_db'
        # }
    }

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': 'memcache:11211',
        'TIMEOUT': CACHE_TIMEOUT,
        'OPTIONS': {
            'server_max_value_length': 1024 * 1024 * 10
        }
    }
}

TEMPLATE_DEBUG = DEBUG

# https://stackoverflow.com/questions/67783120/warning-auto-created-primary-key-used-when-not-defining-a-primary-key-type-by
# Evita warnings no log quando rodar runcrons
# Acho que pode ter relação com deixar a versão do Django subir livremente
DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'
