from django.test import TestCase

from analises.models import AnaliseAvancadaForm
from modelagem.models import CasaLegislativa


class AnaliseAvancadaFormTest(TestCase):

    def test_form_sem_problemas(self):
        casa = self._casa('cdep')
        # como datas vêm direto do form da view, os anos são string (não int)
        params = {'ano_ini_era': '2009', 'ano_fim_era': '2010'}
        form = AnaliseAvancadaForm(params, casa)
        problemas = form.problemas()
        self.assertFalse(problemas)

    def test_form_sem_params_sem_problema(self):
        casa = self._casa('cdep')
        params = {}
        form = AnaliseAvancadaForm(params, casa)
        problemas = form.problemas()
        self.assertFalse(problemas)

    def test_form_com_problema_ano_ini_maior_que_ano_fim(self):
        casa = self._casa('cdep')
        params = {'ano_ini_era': '2020', 'ano_fim_era': '2010'}
        form = AnaliseAvancadaForm(params, casa)
        problemas = form.problemas()
        self.assertTrue(problemas)
        self.assertEquals(problemas[0], AnaliseAvancadaForm.PROBLEMA_INICIO_ERA_POSTERIOR_AO_FIM_ERA)

    def test_form_com_era_maior_que_dez_anos_para_cdep(self):
        casa = self._casa('cdep')
        params = {'ano_ini_era': '2000', 'ano_fim_era': '2020'}
        form = AnaliseAvancadaForm(params, casa)
        problemas = form.problemas()
        self.assertTrue(problemas)
        self.assertEquals(problemas[0], AnaliseAvancadaForm.PROBLEMA_ERA_CDEP_MUITO_GRANDE)

    def test_form_com_era_maior_que_dez_anos_para_outra_casa_qualquer(self):
        casa = self._casa('outra_casa_qualquer')
        params = {'ano_ini_era': '2000', 'ano_fim_era': '2020'}
        form = AnaliseAvancadaForm(params, casa)
        problemas = form.problemas()
        self.assertFalse(problemas)

    def test_form_apenas_ini_era_informado(self):
        casa = self._casa('cdep')
        params = {'ano_ini_era': '2000'}
        form = AnaliseAvancadaForm(params, casa)
        problemas = form.problemas()
        self.assertTrue(problemas)
        self.assertEquals(problemas[0], AnaliseAvancadaForm.ERA_PRECISO_DOS_DOIS_EXTREMOS)

    def test_form_apenas_fim_era_informado(self):
        casa = self._casa('cdep')
        params = {'ano_fim_era': '2000'}
        form = AnaliseAvancadaForm(params, casa)
        problemas = form.problemas()
        self.assertTrue(problemas)
        self.assertEquals(problemas[0], AnaliseAvancadaForm.ERA_PRECISO_DOS_DOIS_EXTREMOS)

    def _casa(self, nome_curto):
        casa = CasaLegislativa()
        casa.nome_curto = nome_curto
        return casa
