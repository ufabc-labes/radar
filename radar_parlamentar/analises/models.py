# Copyright (C) 2012, Leonardo Leite
#
# This file is part of Radar Parlamentar.
#
# Radar Parlamentar is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Radar Parlamentar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Radar Parlamentar.  If not, see <http://www.gnu.org/licenses/>.


class AnaliseTemporal:

    def __init__(self):
        self.casa_legislativa = None
        self.periodicidade = None
        self.analises_periodo = []
        self.votacoes = []
        self.total_votacoes = 0
        self.palavras_chaves = []


class AnalisePeriodo:

    def __init__(self):
        self.casa_legislativa = None
        self.periodo = None  # PeriodoCasaLegislativa
        self.partidos = []
        self.votacoes = []
        self.num_votacoes = 0
        self.chefes_executivo = []

        self.pca = None

        self.tamanhos_partidos = {}  # partido => int
        self.coordenadas_partidos = {}  # partdo => [x,y]
        # TODO: coordenadas_partidos should be partido.nome => [x,y]

        self.presencas_parlamentares = {}  # parlamentar.parlamentar_id => boolean
        self.coordenadas_parlamentares = {}  # parlamentar.parlamentar_id => [x,y]

        # partido.nome => lista de parlamentares do partido
        # (independente de periodo).
        self.parlamentares_por_partido = {}


class AnaliseAvancadaForm:

    PROBLEMA_INICIO_ERA_POSTERIOR_AO_FIM_ERA = 'Início da era analisada não pode ser posterior ao fim da era'
    PROBLEMA_ERA_CDEP_MUITO_GRANDE = 'Era não pode ser maior que 10 anos para a Câmara dos Deputados'
    ERA_PRECISO_DOS_DOIS_EXTREMOS = 'Início e fim de era devem ser informados conjuntamente'
    LIMITE_TAMANHO_ERA_CDEP = 10

    def __init__(self, values, casa_legislativa):
        self.ano_ini_era = values.get('ano_ini_era')
        self.ano_fim_era = values.get('ano_fim_era')
        self.casa_legislativa = casa_legislativa

    """Retorna uma lista de problemas (cada problema é uma string)"""
    def problemas(self):

        problemas = []

        if not self.ano_ini_era and not self.ano_fim_era:
            return problemas

        if not self.ano_ini_era or not self.ano_fim_era:
            problemas.append(AnaliseAvancadaForm.ERA_PRECISO_DOS_DOIS_EXTREMOS)
            return problemas

        if self.ano_ini_era and self.ano_fim_era:
            if self.ano_ini_era > self.ano_fim_era:
                problemas.append(AnaliseAvancadaForm.PROBLEMA_INICIO_ERA_POSTERIOR_AO_FIM_ERA)

        if self.casa_legislativa.nome_curto == 'cdep':
            intervalo = int(self.ano_fim_era) - int(self.ano_ini_era)
            if intervalo > AnaliseAvancadaForm.LIMITE_TAMANHO_ERA_CDEP:
                problemas.append(AnaliseAvancadaForm.PROBLEMA_ERA_CDEP_MUITO_GRANDE)

        return problemas
