#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# Gera a maioria das variáveis importantes que precisamos, diferenciando se
# estamos rodando no CI ou não.
set -eufCo pipefail
export SHELLOPTS	# propagate set to children by default
IFS=$'\t\n'

# Referências de Diretórios
ROOT_DIR="$(git rev-parse --show-toplevel)"
export ROOT_DIR="${ROOT_DIR}"
export SCRIPTS="${ROOT_DIR}/scripts"
export CI_SCRIPTS="${SCRIPTS}/ci"
export LINTERS="${SCRIPTS}/linter"
export DOCKERFILES="${SCRIPTS}/dockerfiles"
export RADAR_TEMP_DIR="${ROOT_DIR}/.tmp_ci_radar/"

# Referências pra nome das imagens docker
export REGISTRY="${CI_REGISTRY_IMAGE:-registry.gitlab.com/radar-parlamentar/radar}"
export IMAGEM_CI="${REGISTRY}/ci"
export IMAGEM_NGINX="${REGISTRY}/nginx"
export IMAGEM_TESTE="${REGISTRY}/teste"
export IMAGEM_PRODUCAO="${REGISTRY}/producao"

# Encontrando último commit com alteração de cada dockerfile
# Primeiro da imagem de CI
COMMIT="$(git log -n1 --pretty="format:%H" "${DOCKERFILES}/CI")"
export ULTIMA_TAG_CI="${COMMIT:0:8}"

# descartando a variável
unset COMMIT

if [[ "true" == "${GITLAB_CI:-false}" ]]; then
	# Login in gitlab docker registry
	echo "${CI_JOB_TOKEN}" \
		| \
		docker login \
		-u gitlab-ci-token \
		--password-stdin \
		"${CI_REGISTRY}"
	
	# Define a TAG a ser utilizada
	if [[ "${CI_COMMIT_REF_NAME}" == "master" ]]; then
		# Se estivermos na branch master, usamos o hash do commit.
		TAG="${CI_COMMIT_SHORT_SHA}"
	else
		# Do contrário usamos o nome da branch
		TAG="${CI_COMMIT_REF_NAME/\//}"
	fi
	
	# Define mais algumas labels da imagem docker do radar
	RADAR_VERSION="$(git log -n1 --pretty="format:%H")"
	VERSION_DATE="$(TZ='America/Sao_Paulo' \
		date -d @"$(git log -n1 --format='%at')" "+%d/%m/%Y")"

else
	# Se estivermos localmente usamos o nome da branch mesmo
	TAG="dev-local"

	# Define mais algumas labels da imagem docker do radar
	RADAR_VERSION="dev-local"
	VERSION_DATE="$(TZ='America/Sao_Paulo' \
		date "+%d/%m/%Y")"
fi

export TAG
export RADAR_VERSION
export VERSION_DATE
