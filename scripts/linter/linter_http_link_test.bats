#!/usr/bin/env bats
# vim: ft=sh:ts=8:sw=8:noet

# General tests
@test "linter_http_link: is a function" {
	run env -i bash -c "source ./fun/linter_http_link.sh \
				&& type -t linter_http_link"
	[[ "${lines[0]}" = 'function' ]]
	[[ 0 -eq "${status}" ]]
}

@test "linter_http_link: does not set extra envvars after being sourced" {
	run env -i bash -c "env_pre=\$(export); \
		source ./fun/linter_http_link.sh; \
		env_post=\$(export); \
		diff <(echo \"\${env_pre}\" | grep -v _ps1_cmd) <(echo \"\${env_post}\" | grep -v _ps1_cmd)"
	[[ "x${output}x" = 'xx' ]]
	[[ 0 -eq "${status}" ]]
}

@test "linter_http_link: does not set extra options after being run" {
	run env bash -c "opts_pre=\$(set +o); \
		source ./fun/linter_http_link.sh; \
		linter_http_link; \
		opts_post=\$(set +o); \
		diff <(echo \"\${opts_pre}\" | grep -v _ps1_cmd) <(echo \"\${opts_post}\" | grep -v _ps1_cmd)"
	[[ "${output}" = "linter_http_link: error: can't check url if there is none!" ]]
	[[ 0 -eq "${status}" ]]
}

# Invocation tests
# Two tests needed here since bats merges stderr and stdout, hence we check both individually
@test "linter_http_link: fails when curl is not installed, writes nothing to stdout" {
	run env -i bash -c "source ./fun/linter_http_link.sh \
				&& unset PATH \
				&& linter_http_link 2>/dev/null"
	[[ "x${output}x" = 'xx' ]]
	[[ 1 -eq "${status}" ]]
}
@test "linter_http_link: fails when curl is not installed, writes errors to stderr" {
	run env -i bash -c "source ./fun/linter_http_link.sh \
				&& unset PATH \
				&& linter_http_link >/dev/null"
	[[ "${output}" = 'linter_http_link: error: curl is not installed!' ]]
	[[ 1 -eq "${status}" ]]
}

# Two tests needed here since bats merges stderr and stdout, hence we check both individually
@test "linter_http_link: fails w/o arguments, writes nothing to stdout" {
	run env -i bash -c "source ./fun/linter_http_link.sh \
				&& linter_http_link 2>/dev/null"
	[[ "x${output}x" = 'xx' ]]
	[[ 42 -eq "${status}" ]]
}
@test "linter_http_link: fails w.o arguments, writes errors to stderr" {
	run env -i bash -c "source ./fun/linter_http_link.sh \
				&& linter_http_link >/dev/null"
	[[ "${output}" = "linter_http_link: error: can't check url if there is none!" ]]
	[[ 42 -eq "${status}" ]]
}

# Two tests needed here since bats merges stderr and stdout, hence we check both individually
@test "linter_http_link: fails w/ when scheme is absent, writes nothing to stdout" {
	run bash -c "source ./fun/linter_http_link.sh \
			&& linter_http_link 'example.net' 2>/dev/null"
	[[ "x${output}x" = 'xx' ]]
	[[ 42 -eq "${status}" ]]
}
@test "linter_http_link: fails w/ when scheme is absent, writes error to stderr" {
	run bash -c "source ./fun/linter_http_link.sh \
			&& linter_http_link 'example.net' >/dev/null"
	echo "z${output}z"
	[[ "${lines[0]}" = "linter_http_link: error: URL scheme should be present!" ]]
	[[ 42 -eq "${status}" ]]
}

# Two tests needed here since bats merges stderr and stdout, hence we check both individually
@test "linter_http_link: fails w/ when scheme is not https://, writes nothing to stdout" {
	run bash -c "source ./fun/linter_http_link.sh \
			&& linter_http_link 'gopher://hello-there' 2>/dev/null"
	[[ "x${output}x" = 'xx' ]]
	[[ 42 -eq "${status}" ]]
}
@test "linter_http_link: fails w/ when scheme is not https://, writes error to stderr" {
	run bash -c "source ./fun/linter_http_link.sh \
			&& linter_http_link 'http://' >/dev/null"
	[[ "${lines[0]}" = "linter_http_link: error: URL scheme should be https, got 'http'!" ]]
	[[ 42 -eq "${status}" ]]
}

# Gitlab related invocations
# Two tests needed here since bats merges stderr and stdout, hence we check both individually
@test "linter_http_link: gitlab: fails w/o token envvar, writes nothing to stdout" {
	run bash -c "source ./fun/linter_http_link.sh \
			&& unset GITLAB_TOKEN \
			&& linter_http_link 'https://gitlab.otters.xyz/product/systems/functionarium/blob/master/README_NOT.md' 2>/dev/null"
	[[ "x${output}x" = 'xx' ]]
	[[ 42 -eq "${status}" ]]
}
@test "linter_http_link: gitlab: fails w/o token envvar, writes proper error to stderr" {
	run bash -c "source ./fun/linter_http_link.sh \
			&& unset GITLAB_TOKEN \
			&& linter_http_link 'https://gitlab.otters.xyz/product/systems/functionarium/blob/master/README_NOT.md' >/dev/null"
	[[ "${lines[0]}" = "linter_http_link: error: Requested gitlab url without exported GITLAB_TOKEN!" ]]
	[[ 42 -eq "${status}" ]]
}

# Two tests needed here since bats merges stderr and stdout, hence we check both individually
@test "linter_http_link: gitlab: fails w/ non-master branch, writes nothing to stdout" {
	run bash -c "source ./fun/linter_http_link.sh \
			&& export GITLAB_TOKEN=notneeded \
			&& linter_http_link 'https://gitlab.otters.xyz/product/systems/functionarium/blob/something/with/slashes/README.md' 2>/dev/null"
	[[ "x${output}x" = 'xx' ]]
	[[ 42 -eq "${status}" ]]
}
@test "linter_http_link: gitlab: fails w/ non-master branch, writes proper error to stderr" {
	run bash -c "source ./fun/linter_http_link.sh \
			&& export GITLAB_TOKEN=notneeded \
			&& linter_http_link 'https://gitlab.otters.xyz/product/systems/functionarium/blob/not/sure/if/branch/or/paths/README.md' >/dev/null"
	[[ "${lines[0]}" = "linter_http_link: error: only master branches are supported for gitlab URLs"* ]]
	[[ 42 -eq "${status}" ]]
}

# Two tests needed here since bats merges stderr and stdout, hence we check both individually
@test "linter_http_link: gitlab: fails w/ query in url, writes nothing to stdout" {
	run bash -c "source ./fun/linter_http_link.sh \
			&& export GITLAB_TOKEN=notneeded \
			&& linter_http_link 'https://gitlab.otters.xyz/product/systems/functionarium/blob/master/README.md?query' 2>/dev/null"
	[[ "x${output}x" = 'xx' ]]
	[[ 42 -eq "${status}" ]]
}
@test "linter_http_link: gitlab: fails w/ query in url, writes proper error to stderr" {
	run bash -c "source ./fun/linter_http_link.sh \
			&& export GITLAB_TOKEN=notneeded \
			&& linter_http_link 'https://gitlab.otters.xyz/product/systems/functionarium/blob/master/README.md?query' >/dev/null"
	[[ "${lines[0]}" = "linter_http_link: error: queries are not supported for gitlab URLs!" ]]
	[[ 42 -eq "${status}" ]]
}

# Two tests needed here since bats merges stderr and stdout, hence we check both individually
@test "linter_http_link: gitlab: fails w/ absent file, writes nothing to stdout" {
	run bash -c "source ./fun/linter_http_link.sh \
			&& export GITLAB_TOKEN=${GITLAB_TOKEN:-SetMeForTests} \
			&& linter_http_link 'https://gitlab.otters.xyz/product/systems/functionarium/blob/master/README_NOT.md' 2>/dev/null"
	[[ "x${output}x" = 'xx' ]]
	[[ 1 -eq "${status}" ]]
}
@test "linter_http_link: gitlab: fails w/ absent file, writes proper error to stderr" {
	run bash -c "source ./fun/linter_http_link.sh \
			&& export GITLAB_TOKEN=${GITLAB_TOKEN:-SetMeForTests} \
			&& linter_http_link 'https://gitlab.otters.xyz/product/systems/functionarium/blob/master/README_NOT.md' >/dev/null"
	[[ "${lines[0]}" = "linter_http_link: error: Error code: '404', url: 'https://gitlab.otters.xyz/product/systems/functionarium/blob/master/README_NOT.md', checked via api url 'https://gitlab.otters.xyz/api/v4/projects/product%2Fsystems%2Ffunctionarium/repository/files/README_NOT.md?ref=master'!" ]]
	[[ 1 -eq "${status}" ]]
}

@test "linter_http_link: gitlab: succeeds w/ existing file" {
	run bash -c "source ./fun/linter_http_link.sh \
			&& linter_http_link 'https://gitlab.otters.xyz/product/systems/functionarium/blob/master/README.md' 2>/dev/null"
	[[ "x${output}x" = 'xx' ]]
	[[ 0 -eq "${status}" ]]
}

# GitHub related invocations
# Two tests needed here since bats merges stderr and stdout, hence we check both individually
@test "linter_http_link: github: fails w/o token envvar, writes nothing to stdout" {
	run bash -c "source ./fun/linter_http_link.sh \
			&& unset GITHUB_TOKEN \
			&& linter_http_link 'https://github.com/cabify/functionarium/blob/master/README_NOT.md' 2>/dev/null"
	[[ "x${output}x" = 'xx' ]]
	[[ 42 -eq "${status}" ]]
}
@test "linter_http_link: github: fails w/o token envvar, writes proper error to stderr" {
	run bash -c "source ./fun/linter_http_link.sh \
			&& unset GITHUB_TOKEN \
			&& linter_http_link 'https://github.com/cabify/functionarium/blob/master/README_NOT.md' >/dev/null"
	[[ "${lines[0]}" = "linter_http_link: error: Requested github url without exported GITHUB_TOKEN!" ]]
	[[ 42 -eq "${status}" ]]
}

# Two tests needed here since bats merges stderr and stdout, hence we check both individually
@test "linter_http_link: github: fails w/ non-master branch, writes nothing to stdout" {
	run bash -c "source ./fun/linter_http_link.sh \
			&& export GITHUB_TOKEN=notneeded \
			&& linter_http_link 'https://github.com/cabify/functionarium/blob/something/with/slashes/README.md' 2>/dev/null"
	[[ "x${output}x" = 'xx' ]]
	[[ 42 -eq "${status}" ]]
}
@test "linter_http_link: github: fails w/ non-master branch, writes proper error to stderr" {
	run bash -c "source ./fun/linter_http_link.sh \
			&& export GITHUB_TOKEN=notneeded \
			&& linter_http_link 'https://github.com/cabify/functionarium/blob/not/sure/if/branch/or/paths/README.md' >/dev/null"
	[[ "${lines[0]}" = "linter_http_link: error: only master branches are supported for github URLs"* ]]
	[[ 42 -eq "${status}" ]]
}

# Two tests needed here since bats merges stderr and stdout, hence we check both individually
@test "linter_http_link: github: fails w/ query in url, writes nothing to stdout" {
	run bash -c "source ./fun/linter_http_link.sh \
			&& export GITHUB_TOKEN=notneeded \
			&& linter_http_link 'https://github.com/cabify/functionarium/blob/master/README.md?query' 2>/dev/null"
	[[ "x${output}x" = 'xx' ]]
	[[ 42 -eq "${status}" ]]
}
@test "linter_http_link: github: fails w/ query in url, writes proper error to stderr" {
	run bash -c "source ./fun/linter_http_link.sh \
			&& export GITHUB_TOKEN=notneeded \
			&& linter_http_link 'https://github.com/cabify/functionarium/blob/master/README.md?query' >/dev/null"
	[[ "${lines[0]}" = "linter_http_link: error: queries are not supported for github URLs!" ]]
	[[ 42 -eq "${status}" ]]
}

# Two tests needed here since bats merges stderr and stdout, hence we check both individually
@test "linter_http_link: github: fails w/ absent file, writes nothing to stdout" {
	run bash -c "source ./fun/linter_http_link.sh \
			&& export GITHUB_TOKEN=${GITHUB_TOKEN:-SetMeForTests} \
			&& linter_http_link 'https://github.com/cabify/functionarium/blob/master/README_NOT.md' 2>/dev/null"
	[[ "x${output}x" = 'xx' ]]
	[[ 1 -eq "${status}" ]]
}
@test "linter_http_link: github: fails w/ absent file, writes proper error to stderr" {
	run bash -c "source ./fun/linter_http_link.sh \
			&& export GITHUB_TOKEN=${GITHUB_TOKEN:-SetMeForTests} \
			&& linter_http_link 'https://github.com/cabify/functionarium/blob/master/README_NOT.md' >/dev/null"
	[[ "${lines[0]}" = "linter_http_link: error: Error code: '404', url: 'https://github.com/cabify/functionarium/blob/master/README_NOT.md', checked via api url 'https://api.github.com/repos/cabify/functionarium/contents/README_NOT.md'!" ]]
	[[ 1 -eq "${status}" ]]
}

@test "linter_http_link: github: succeeds w/ existing file" {
	run bash -c "source ./fun/linter_http_link.sh \
			&& linter_http_link 'https://github.com/cabify/functionarium/blob/master/README.md'"
	[[ "x${output}x" = 'xx' ]]
	[[ 0 -eq "${status}" ]]
}

# Generic invocations
@test "linter_http_link: generic: fails w/ nonexisitng file" {
	run bash -c "source ./fun/linter_http_link.sh \
			&& linter_http_link 'https://www.google.com/hamsters.txt'"
	[[ "${lines[0]}" = "linter_http_link: error: Error code: '404', url: 'https://www.google.com/hamsters.txt', checked directly!" ]]
	[[ 1 -eq "${status}" ]]
}

@test "linter_http_link: generic: succeeds w/ existing file" {
	run bash -c "source ./fun/linter_http_link.sh \
			&& linter_http_link 'https://www.google.com/robots.txt'"
	[[ "x${output}x" = 'xx' ]]
	[[ 0 -eq "${status}" ]]
}

