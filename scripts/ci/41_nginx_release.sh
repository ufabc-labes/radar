#!/bin/bash
# vim: ai:ts=8:sw=8:noet

# Run this script only when in CI
ci_only

# Faz o pull da imagem, caso ela exista, para acelerar o processo.
docker pull "${IMAGEM_NGINX}:${TAG}" || \
	docker pull ${IMAGEM_NGINX}:latest || \
	true

# Constrói a imagem novamente "por garantia" de que será a versão mais atualizada.
docker build \
	--cache-from "${IMAGEM_NGINX}:${TAG}" \
	-t "${IMAGEM_NGINX}:${TAG}" \
	-t "${IMAGEM_NGINX}:latest" \
	-f "${DOCKERFILES}/NGINX" \
	.

docker push "${IMAGEM_NGINX}:${TAG}"
docker push "${IMAGEM_NGINX}:latest"
