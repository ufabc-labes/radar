#!/bin/bash
# vim: ai:ts=8:sw=8:noet

# Run this script only when in CI
ci_only

# Faz o pull da imagem, caso ela exista, para acelerar o processo.
docker pull "${IMAGEM_TESTE}:${TAG}" || \
	docker pull "${IMAGEM_PRODUCAO}:latest"

# Construindo a imagem de produção com base na imagem de teste
docker build \
	--cache-from "${IMAGEM_TESTE}:${TAG}" \
	-t "${IMAGEM_TESTE}:latest" \
	-t "${IMAGEM_PRODUCAO}:${TAG}" \
	-t "${IMAGEM_PRODUCAO}:latest" \
	-f "${DOCKERFILES}/Main" \
	--build-arg RADAR_VERSION="${RADAR_VERSION}" \
	--build-arg VERSION_DATE="${VERSION_DATE}" \
	.

docker push "${IMAGEM_TESTE}:latest"
docker push "${IMAGEM_PRODUCAO}:${TAG}"
docker push "${IMAGEM_PRODUCAO}:latest"
