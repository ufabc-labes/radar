#!/bin/bash
# vim: ai:ts=8:sw=8:noet

# Tenta baixar alguma(s) imagem(ns) para usar o cache dos layers e acelerar o
# build.
docker pull "${IMAGEM_NGINX}:${TAG}" || \
    docker pull "${IMAGEM_NGINX}:latest" ||
    true

if [[ -n "$(docker image ls --filter "reference=${IMAGEM_NGINX}" -q)" ]]; then
	docker build \
		--cache-from "$(docker image ls --filter "reference=${IMAGEM_NGINX}" --format "{{.CreatedAt}} {{.Repository}}:{{.Tag}}" | sort | tail -n1  | awk '{ print $5 }')" \
		-t "${IMAGEM_NGINX}:${TAG}" \
		-f "${DOCKERFILES}/NGINX" \
		.
else
	docker build \
		--cache-from "${IMAGEM_NGINX}:latest" \
		-t "${IMAGEM_NGINX}:${TAG}" \
		-f "${DOCKERFILES}/NGINX" \
		.
fi

if [[ "${GITLAB_CI:-false}" == "true" ]]; then
	docker push "${IMAGEM_NGINX}:${TAG}"
fi
