#!/bin/bash
# vim: ai:ts=8:sw=8:noet
COMMIT_ATUAL="$(git log -n1 --pretty="format:%h")"
MIGRATIONS_MODIFICADAS="$(git show origin/master.."${COMMIT_ATUAL}" \
	--pretty='format:' \
	--name-status | sort -u | grep -E '^M.*/migrations/' || echo "OK")"

if [ ! "${MIGRATIONS_MODIFICADAS}" == "OK" ]; then
	echo "Uma ou mais migrations foram modificadas manualmente."
	echo "Este é um procedimento que não pode ocorrer."
	echo "Você deveria utilizar o makemigrations para criar novas migrations."
	echo "Migrations alteradas:"
	echo "${MIGRATIONS_MODIFICADAS}"
	echo "Falhando o CI até que não existam migrations alteradas."
	exit 42
fi

echo "Nenhuma migration foi modificada neste Merge Request. Tudo ok!"
