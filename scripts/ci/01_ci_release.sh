#!/bin/bash
# vim: ai:ts=8:sw=8:noet

# Run this script only when in CI
ci_only

# Faz o pull da imagem, caso ela exista, para acelerar o processo.
docker pull "${IMAGEM_CI}:${TAG}" || true
# Constrói a imagem novamente "por garantia" de que será a versão mais atualizada.
docker build \
	-t "${IMAGEM_CI}:${ULTIMA_TAG_CI}" \
	-f "${DOCKERFILES}/CI" \
	.

docker tag \
	"${IMAGEM_CI}:${ULTIMA_TAG_CI}" \
	"${IMAGEM_CI}:latest"

docker push "${IMAGEM_CI}:${ULTIMA_TAG_CI}"
docker push "${IMAGEM_CI}:latest"
